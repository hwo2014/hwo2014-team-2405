class Robolution
  def initialize(messenger)
    @messenger = messenger
    @messenger.add_observer(self)
    @turbo = Turbo.new(false, 0, 0.0)
    messenger.set_turbo_callback lambda { |available, duration, factor|
      @turbo.available = available
      @turbo.duration = duration
      @turbo.factor = factor
      puts 'Turbo available!'
    }
  end

  def play_quick_match()
    puts 'playing quick match'
    @messenger.send_join
  end

  # gets called when a carPositions message was received
  def update
    @messenger.send_throttle(0.6)
  end
end