require 'logger'
require 'csv'

class HWOLogger < Logger
  def initialize(file_output)
    if file_output
      id = Dir.glob('logs/*.log').count + 1
      file = File.open("logs/#{id}.log", 'w')
      super(file)
      self.level = Logger::INFO
      self.formatter = proc { |severity, datetime, progname, msg|
      "#{msg}\n"
      }
      @csv = CSV.open("logs/#{id}.csv", "wb")
      @columns_defined = false
    else
      super(STDOUT)
      self.level = Logger::FATAL
    end
  end

  def debug_info(quick_race, track, laps, color, length, width, guide_flag_position)
    info("DateTime: #{DateTime.now}")
    info("Quick Race: #{quick_race}")
    info("Track: #{track}")
    info("Laps: #{laps}")
    info("Color: #{color}")
    info("Length: #{length}")
    info("Width: #{widhth}")
    info("Guide Flag Position: #{guide_flag_position}")
  end

  def sent(msg)
    self.info("[Sent] #{msg}")
  end

  def received(msg)
    self.info("[Received] #{msg}")
  end

  def data(car_positions)
    if $debug
      if @columns_defined == false
        write_column_definitions(car_positions)
        @columns_defined = true
      end
      data = [car_positions.tick]
      car_positions.car_positions.each do |car_position|
        new_data = car_position.values
        index_piece_position = car_position.members.find_index(:piece_position)
        new_data[index_piece_position] = car_position.piece_position.values
        new_data.flatten!
        data += new_data
      end
      @csv << data
    end
    return nil
  end

  def write_column_definitions(car_positions)
    columns = [:tick]
    car_positions.car_positions.each do |car_position|
      new_columns = car_position.members
      new_columns[new_columns.find_index(:piece_position)] = car_position.piece_position.members
      new_columns.flatten!.map! {|column| "#{car_position.color}_#{column}"}
      columns += new_columns
    end
    @csv << columns
  end
end