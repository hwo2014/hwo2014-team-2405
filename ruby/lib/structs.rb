Turbo = Struct.new(:available, :duration, :factor)

Track = Struct.new(:track_pieces, :lanes)
TrackPieceStraight = Struct.new(:length, :switch)
TrackPieceBend = Struct.new(:radius, :angle, :switch)
Lane = Struct.new(:index, :distance_from_center)

# types: quick, qualifying, race (if qualifying: max_lap_time_ms = duration_ms)
RaceSession = Struct.new(:type, :laps, :max_laptime_ms)

Car = Struct.new(:name, :color, :dimensions, :position)
Dimensions = Struct.new(:length, :width, :guide_flag_position)

CarPositions = Struct.new(:car_positions, :tick)
CarPosition = Struct.new(:color, :angle, :piece_position, :lap)
PiecePosition = Struct.new(:piece_index, :piece_distance, :start_lane, :end_lane)