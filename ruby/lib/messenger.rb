require 'socket'
require 'json'
require 'observer'

class Messenger
  include Observable

  def initialize(server_host, server_port, bot_name, bot_key)
    @server_host = server_host
    @server_port = server_port
    @bot_name = bot_name
    @bot_key = bot_key
    @tcp = TCPSocket.new(server_host, server_port)

    @turbo_callback = nil
    @crash_callback = nil
    @spawn_callback = nil
    @dnf_callback = nil

    react_to_messages_from_server
  end

  def set_turbo_callback(callback)
    @turbo_callback = callback
  end

  def set_crash_callback(callback)
    @crash_callback = callback
  end

  def set_spawn_callback(callback)
    @spawn_callback = callback
  end

  def set_dnf_callback(callback)
    @dnf_callback = callback
  end

  def react_to_messages_from_server
    @thread = Thread.new {
      while json = @tcp.gets
        message = JSON.parse(json)
        msg_type = message['msgType']
        msg_data = message['data']
        case msg_type
          when 'carPositions'
            car_positions = CarPositions.new(Array.new, message['gameTick'])
            msg_data.each do |pos_data|
              car_position = CarPosition.new
              car_position.color = pos_data['id']['color']
              car_position.angle = pos_data['angle']
              car_position.piece_position = PiecePosition.new
              car_position.piece_position.piece_index = pos_data['piecePosition']['pieceIndex']
              car_position.piece_position.piece_distance = pos_data['piecePosition']['inPieceDistance']
              car_position.piece_position.start_lane = pos_data['piecePosition']['lane']['startLaneIndex']
              car_position.piece_position.end_lane = pos_data['piecePosition']['lane']['endLaneIndex']
              car_position.lap = pos_data['lap']
              # TODO: Somehow lap is always nil; gotta investigate further
              car_position.lap ||= 0 if car_position.lap == nil
              car_positions.car_positions << car_position
            end
            $logger.data(car_positions)
            changed
            notify_observers
          else
            $logger.received(msg_type)
            case msg_type
              when 'join'
              when 'yourCar'
                puts "Color: #{msg_data['color']}"
              when 'gameInit'
                # TODO
              when 'gameStart'
                puts 'Game started!'
              when 'turboAvailable'
                if @turbo_callback != nil
                  @turbo_callback.call(true, msg_data['turboDurationTicks'], msg_data['turboFactor'])
                else
                  warn 'turbo_callback is not set!'
                end
              when 'crash'
                if @crash_callback != nil
                  @crash_callback.call(true, msg_data['turboDurationTicks'], msg_data['turboFactor'])
                else
                  warn 'crash_callback is not set!'
                end
              when 'spawn'
                if @spawn_callback != nil
                  @spawn_callback.call(true, msg_data['turboDurationTicks'], msg_data['turboFactor'])
                else
                  warn 'spawn_callback is not set!'
                end
              when 'dnf'
                if @dnf_callback != nil
                  @dnf_callback.call(msg_data['car']['color'])
                else
                  warn 'dnf_callback is not set!'
                end
              when 'gameEnd'
                puts 'Race ended'
              when 'error'
                puts "ERROR: #{msg_data}"
            end
            send_ping
        end
      end
    }
  end

  def wait_for_end
    @thread.join
  end

  def send_join
    @tcp.puts make_msg("join", {:name => @bot_name, :key => @bot_key})
    $logger.sent('join')
  end

  def send_createRace(track_name = nil, password = nil, car_count = 2)
    data = { :botId => {name: @bot_name, key: @bot_key} }
    data[:trackName] = track_name if track_name != nil
    data[:password] = password if password != nil
    data[:carCount] = car_count
    @tcp.puts make_msg("throttle", throttle)
  end

  def send_joinRace(trackname = nil, password = nil, carcount = 2)

  end

  def send_throttle(throttle)
    @tcp.puts make_msg("throttle", throttle)
    $logger.sent('throttle')
  end

  def send_ping
    @tcp.puts make_msg("ping", {})
  end

  def make_msg(msg_type, data)
    JSON.generate({:msgType => msg_type, :data => data})
  end
end