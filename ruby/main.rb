require_relative 'lib/robolution'
require_relative 'lib/messenger'
require_relative 'lib/structs'
require_relative 'lib/hwo_logger'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

if ARGV[4] == 'debug'
  $debug = true
  require 'pry'
  puts 'Enabled Debugging!'
  $logger = HWOLogger.new(true)
else
  $logger = HWOLogger.new(false)
end

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

messenger = Messenger.new(server_host, server_port, bot_name, bot_key)
bot = Robolution.new(messenger)
bot.play_quick_match
messenger.wait_for_end